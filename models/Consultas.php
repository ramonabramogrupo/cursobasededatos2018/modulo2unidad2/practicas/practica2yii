<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultas".
 *
 * @property int $id
 * @property string $texto
 * @property string $tabla
 */
class Consultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto', 'tabla'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
            'tabla' => 'Tabla',
        ];
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Consultas;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    
    public function actionIndex(){
     $dataProvider = new ActiveDataProvider([
            'query' => Consultas::find(),
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);   
    }
    public function actionIndexold()
    {
        
        $consultas=Consultas::find()->asArray()->all();
        /*$consultas=[
            // c1
            [
                "texto"=>"Mostrar todos los campos y todos los registros de la tabla empleado",
                "tabla"=>"emple",
            ],
            // c2
            [
                "texto"=>"Mostrar todos los campos y todos los registros de la tabla departamento",
                "tabla"=>"depart",
            ],
            // c3
            [
                "texto"=>"Mostrar el apellido y oficio de cada empleado.",
                "tabla"=>"emple",
            ],
            // c4
            [
              "texto"=>"Mostrar localización y número de cada departamento",
              "tabla"=>"depart",
            ],
            // c5
            [
              "texto"=>"Mostrar el número, nombre y localización de cada departamento",
              "tabla"=>"depart",
            ],
            // c6
            [
              "texto"=>"Indicar el número de empleados que hay",
              "tabla"=>"emple",
            ],
            // c7
            [
              "texto"=>"Datos de los empleados ordenados por apellido de forma ascendente",
              "tabla"=>"emple",
            ],
            // c8
            [
              "texto"=>"Datos de los empleados ordenados por apellido de forma descendente",
              "tabla"=>"emple",
            ],
             // c9
            [
              "texto"=>"Indicar el numero de departamentos que hay",
              "tabla"=>"depart",
            ],
            // c10
            [
              "texto"=>"Datos de los empleados ordenados por número de departamento descendentemente.",
              "tabla"=>"emple",
            ],
               // c11
            [
              "texto"=>"Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente.",
              "tabla"=>"emple",
            ],
            // c12
             [
              "texto"=>"Datos de los empleados ordenados por número de departamento descendentemente y por apellido
ascendentemente.",
              "tabla"=>"emple",
            ],
            
              // c13
             [
              "texto"=>"Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.",
              "tabla"=>"emple",
            ],
             // c14
             [
              "texto"=>"Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000.",
              "tabla"=>"emple",
            ],
              // c15
             [
              "texto"=>"Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500.",
              "tabla"=>"emple",
            ],
            // c16
             [
              "texto"=>"Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ.",
              "tabla"=>"emple",
            ],
            // c17
             [
              "texto"=>"Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €",
              "tabla"=>"emple",
            ],
              // c18
             [
              "texto"=>"Seleccionar el apellido y oficio de los empleados del departamento número 20.",
              "tabla"=>"emple",
            ],
             // c19
             [
              "texto"=>"Contar el número de empleados cuyo oficio sea VENDEDOR",
              "tabla"=>"emple",
            ],
             // c20
             [
              "texto"=>"Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.",
              "tabla"=>"emple",
            ],
             // c21
             [
              "texto"=>"Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma ascendente",
              "tabla"=>"emple",
            ],
             // c22
             [
              "texto"=>"Mostrar los apellidos del empleado que mas gana",
              "tabla"=>"emple",
            ],
            // c23
             [
              "texto"=>"Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y oficio de forma ascendente",
              "tabla"=>"emple",
            ],
            // c24
             [
              "texto"=>"Realizar un listado de los distintos meses en que los empleados se han dado de alta",
              "tabla"=>"emple",
            ],
             // c25
             [
              "texto"=>"Realizar un listado de los distintos años  en que los empleados se han dado de alta",
              "tabla"=>"emple",
            ],
             // c26
             [
              "texto"=>"Realizar un listado de los distintos días del mes en que los empleados se han dado de alta",
              "tabla"=>"emple",
            ],
             // c27
             [
              "texto"=>"Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20",
              "tabla"=>"emple",
            ],
              // c28
             [
              "texto"=>"Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece",
              "tabla"=>"emple",
            ],
             // c29
             [
              "texto"=>"Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente",
              "tabla"=>"emple",
            ],
              // c30
             [
              "texto"=>"Listar el número de empleados por departamento. ",
              "tabla"=>"emple",
            ],
             // c31
             [
              "texto"=>"Realizar el mismo comando anterior pero obteniendo una salida como la que vemos ",
              "tabla"=>"emple",
            ],
             // c32
             [
              "texto"=>"Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre",
              "tabla"=>"emple",
            ],
             // c33
             [
              "texto"=>"Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ. Listar el apellido de los empleados",
              "tabla"=>"emple",
            ],
             // c34
             [
              "texto"=>"Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. Listar el apellido de los empleados",
              "tabla"=>"emple",
            ],
             // c35
             [
              "texto"=>"Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. Listar todos los campos de la tabla empleados",
              "tabla"=>"emple",
            ],
        ];*/

        return $this->render('indexOld', [
            "consultas"=>$consultas,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}

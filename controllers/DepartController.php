<?php

namespace app\controllers;

use Yii;
use app\models\Depart;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DepartController implements the CRUD actions for Depart model.
 */
class DepartController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Depart models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Depart::find(),
            
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Depart model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Depart model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Depart();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dept_no]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Depart model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dept_no]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Depart model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Depart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Depart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Depart::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsulta2() {

        /**
         * ActiveQuery
         */
        $a = Depart::find();

        /**
         * ActiveRecord (modelos)
         * lo que tengo es un array de modelos
         */
        $b = $a->all();

        /**
         * ActiveDataProvider
         * 
         */
        $c = new ActiveDataProvider([
            "query" => $a
        ]);

        return $this->render("consulta2", [
                    "activeRecord" => $b,
                    "dp" => $c
        ]);
    }

    // Mostrar localización y número de cada 
    // departamento.
    public function actionConsulta4() {
        $consulta = Depart::find()
                ->select("loc,dept_no");

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render("consulta4", [
                    "sergio" => $dp
        ]);
    }

    // Mostrar el número, nombre y localización 
    // de cada departamento.
    public function actionConsulta5() {
        $consulta = Depart::find();

        // paginar los resultados
        $dp = new ActiveDataProvider([
            "query" => $consulta,
            "pagination" => [
                "pageSize" => 1,
            ],
        ]);

        return $this->render("consulta5", [
                    "sergio" => $dp
        ]);
    }

    public function actionConsulta6() {
        $consulta = Depart::find()->count();


        return $this->render("consulta6", [
                    "d" => $consulta
        ]);
    }

    public function actionConsulta9() {
        $datos = Depart::find()->count();


        $titulo = 'Consulta 9';

        $desc = 'Mostrar el número de departamento';

        return $this->render('consulta9', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

}

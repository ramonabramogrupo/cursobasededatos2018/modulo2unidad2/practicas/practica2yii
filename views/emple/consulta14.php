<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= $titulo ?></h1>
    <h2><?= $texto?> </h2>

    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns' => [            
            'emp_no',
            'apellido'
        ],
    ]); ?>            
            
</div>
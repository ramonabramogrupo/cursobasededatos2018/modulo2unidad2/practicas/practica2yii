<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="emple-index">

    <h1><?= $titulo ?></h1>
    <h2><?= $texto?> </h2>

    <?= GridView::widget([
        'dataProvider' => $datos,
    ]); ?>            
            
</div>
<?php

/* @var $this yii\web\View */
    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\widgets\ListView;

    $this->title = 'Consultas9';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataprovider,
        'columns' => [
            'emp_no',
        ],
    ]); ?>
</div>    
<div class="container"> 
        
       <?= ListView::widget([
            'dataProvider' => $dataprovider,
            'itemView' => '_consulta13',
            ] );
    ?>
</div>

       
<?php
    use yii\helpers\Html;
?>

<div class="row">
    <div class="col-md-2">
        Consulta <?= $numero ?>
    </div>
    <div class="col-md-8">
        <?= $texto ?>
    </div>
    <div class="col-md-2 pad-1">
        <?= Html::a('Ejecutar', [$direccion], ['class' => 'btn btn-lg btn-danger']) ?>
    </div>
</div>


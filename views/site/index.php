
<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <a href="index.php"></a>

    <div class="jumbotron">
        <h1>Practica 2</h1>

        <p class="lead">Consultas</p>
    </div>

    <div class="body-content">
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'texto',
            //'tabla',
            //['class' => 'yii\grid\ActionColumn',
            //    'header' => 'Acciones'
            //],
            ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Consultas',
                    'template' => '{link}',
                    'buttons' => [
                        'link' => function ($url,$model) {
                            return Html::a(Html::a('Consulta', ['/'. $model->tabla .'/consulta' . $model->id],
                                    ['class'=>'btn btn-danger']) ,
                                $url);
                        },
            ],
            ],
        ],
    ]); ?>
    </div>
</div>

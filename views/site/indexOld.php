<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <a href="index.php"></a>

    <div class="jumbotron">
        <h1>Practica 2</h1>

        <p class="lead">Consultas</p>
    </div>

    <div class="body-content">
        <?php
        foreach($consultas as $numero=>$registro){
            $direccion=$registro["tabla"] . "/consulta" . ($numero+1); 
            echo $this->render("_consulta",[
                "numero"=>$numero+1,
                "texto"=>$registro["texto"],
                "direccion"=>$direccion,
            ]);
        }
        ?>
    </div>
</div>
